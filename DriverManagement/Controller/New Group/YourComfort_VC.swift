//
//  YourComfort_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 28/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import Alamofire
import MBProgressHUD


class YourComfort_VC: UIViewController,UITextFieldDelegate {
    //--------------------Transmission type -------------------
    @IBOutlet weak var btn_auto: UIButton!
        {
        didSet{
            btn_auto.addShadowWithExtraWidth(color: .gray)
            
        }
    }
    @IBOutlet weak var btn_manual: UIButton!{
        didSet{
            btn_manual.addShadowWithExtraWidth(color: .gray)
            
        }
    }
    @IBOutlet weak var btn_both: UIButton!{
        didSet{
            
            
            btn_both.addShadowWithExtraWidth(color: .gray)
            
        }
    }
    
    //--------------------Transmission type -------------------
    @IBOutlet weak var btn_sedan: UIButton!{
        didSet{
            btn_sedan.addShadowWithExtraWidth(color: .gray)
        }
    }
    @IBOutlet weak var btn_SUV: UIButton!{
        didSet{
            btn_SUV.addShadowWithExtraWidth(color: .gray)
        }
    }
    @IBOutlet weak var btn_MPV: UIButton!{
        didSet{
            btn_MPV.addShadowWithExtraWidth(color: .gray)
        }
    }
    @IBOutlet weak var btn_pickup: UIButton!{
        didSet{
            btn_pickup.addShadowWithExtraWidth(color: .gray)
        }
    }
    @IBOutlet weak var btn_wagon: UIButton!{
        didSet{
            btn_wagon.addShadowWithExtraWidth(color: .gray)
        }
    }
    @IBOutlet weak var btn_coupe: UIButton!{
        didSet{
            btn_coupe.addShadowWithExtraWidth(color: .gray)
        }
    }
    //--------------------Transmission type -------------------
    @IBOutlet weak var btn_submit: UIButton!{
        didSet{
            btn_submit.addShadowWithExtraWidth(color: .gray)
            btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        }
    }
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var view_Alert_brands: UIView!
    @IBOutlet weak var colView_brands: UICollectionView!
    @IBOutlet weak var btn_done_brands: UIButton!{
        didSet{
            btn_done_brands.layer.cornerRadius = self.btn_done_brands.frame.height / 2
        }
    }
    @IBOutlet weak var view_paging_unfocused: UIView!
    @IBOutlet weak var view_paging_focused: UIView!
    
    @IBOutlet weak var slider_price_range: UISlider!{
        didSet{
            slider_price_range.setThumbImage(image_dot_pink, for: .normal)
            slider_price_range.setThumbImage(image_dot, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    
    @IBOutlet weak var lbl_price_range: UILabel!
    
    var transmissionType = String()
    var arrCarType : [Bool] = []
    var price_range : Float?
    var arrtransmissionType : [Bool] = []
    var arr_car_brands : [[String : Any]] = [[:]]
    var arr_car_types : [[String : Any]] = [[:]]
    
    var arrcheckedM : [Bool] = []
    
    @IBOutlet weak var view_bg_car_brands: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        
        setValues()
        getCars_API()
    }
    
    func setValues(){
        
        //        arr_car_brands = ["Hyundai","Tata","Audi","Bentaly","Kia","Lexus","Mazda","Nissan","Ford","Honda","Maruti","Isuzu","Land Rover","Tata","Maxus","Mercedes Benz","Mitsbishi","Perodua","Cheverlet","Suzuki","Toyota","Mahindra","Proton","Subaru"]
        
        // --------------  Set Transmission type ----------
        for _ in 0 ... 2{
            arrtransmissionType.append(false)
        }
        arrtransmissionType[0] = true
        //----------------------------------------
        
        // --------------  Set Car type ----------
        for _ in 0 ... 5{
            arrCarType.append(false)
        }
        arrCarType[0] = true
        //----------------------------------------
        
        price_range = 500
        transmissionType = "0"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    override func viewWillLayoutSubviews() {
        setUIConfiguration()
        setNavigationBarTransparent()
    }
    
    func setNavigationBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    
    func setUIConfiguration(){
        self.view_Alert_brands.isHidden = true
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2
        view_bg_car_brands.layer.cornerRadius = 24
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("Array checked",self.arrcheckedM)
        
        
    }
    
    @IBAction func submit_Action(_ sender: Any) {

        submitDriver_comfort_API()
        
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func done_Action_brands(_ sender: Any) {
        self.view_Alert_brands.isHidden = true
        
    }
    
    
    @IBAction func close_alert_brands(_ sender: Any) {
        self.view_Alert_brands.isHidden = true
        
    }
    
    @IBAction func select_car_brands(_ sender: Any) {
        self.view_Alert_brands.isHidden = false
        self.colView_brands.reloadData()
    }
    
    @IBAction func slider_changed_priceRange(_ sender: UISlider) {
        price_range = sender.value
        let value = Int(price_range!)
        lbl_price_range.text = String(value)
        print("price_range",price_range)
    }
    
    
    @IBAction func setValue_min(_ sender: Any) {
        
        slider_price_range.value = 0.0
        lbl_price_range.text = "0"
        
    }
    
    @IBAction func setValue_half(_ sender: Any) {
        slider_price_range.value = 5000.0
        lbl_price_range.text = "5000"
    }
    
    
    @IBAction func setValue_max(_ sender: Any) {
        slider_price_range.value = 10000.0
        lbl_price_range.text = "10000"
    }
    
    //--------------- Transmission type ---------------
    
    @IBAction func tranmissionType_clicked(_ sender: UIButton) {
        let selectedIndex =  sender.tag - 10
        
        for i in 0...arrtransmissionType.count - 1{
            let button = self.view.viewWithTag(10 + i) as? UIButton
            if i == selectedIndex && !arrtransmissionType[selectedIndex]{
                arrtransmissionType [i] = true
                sender.setBackgroundImage(UIImage(named: "tab_selected"), for: .normal)
                transmissionType = String(selectedIndex)
            }
            else{
                arrtransmissionType [i] = false
                button!.setBackgroundImage(UIImage(named: "tab_unselected"), for: .normal)
            }
        }
        print("arrtransmissionType",arrtransmissionType)
    }
    
    
    @IBAction func car_type_clicked(_ sender: UIButton) {
        let selectedIndex =  sender.tag - 20
        arrCarType [selectedIndex] = !arrCarType[selectedIndex]
        if arrCarType[selectedIndex]{
            sender.setBackgroundImage(UIImage(named: "tab_selected"), for: .normal)
        }
        else{
            sender.setBackgroundImage(UIImage(named: "tab_unselected"), for: .normal)
        }
        print("arrCarType",arrCarType)
    }
    
    //MARK:- API
    //MARK:-
    func getCars_API()
    {
        
        // * Chechk Whether Network is Unreachable *
        if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
            self.view.MyToast()
            
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        let strUrl = kBaseURL + "cars" as String
        
        AF.request(strUrl, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                progressHUD.hide(animated: true)
                if let value = response.value as? [[String:Any]] {
                    let dicMain : [String:Any] = value[0]
                    guard let status = dicMain["status"] as? String else{
                        return
                    }
                    if status == "success"{
                        self.arr_car_brands = dicMain["brands"] as! [[String:Any]]
                        self.arr_car_types = dicMain["car_types"] as! [[String:Any]]
                        
                        self.arrcheckedM = []
                        for i in 0...self.arr_car_brands.count - 1{
                            self.arrcheckedM.append(false)
                        }
                        self.colView_brands.reloadData()
                    }
                    else{
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async() {
                    print(error.errorDescription?.description as Any)
                    progressHUD.hide(animated: true)
                    
                }
            }
        }
        
    }
    
    
    //MARK:- API
    //MARK:-
    
    func submitDriver_comfort_API()
    {
        //------------ Create Car brand array --------------
        var arr_selected_brand : [String] = []
        for i in 0...self.arrcheckedM.count - 1{
            
            let dic_car_brand : [String : Any] = self.arr_car_brands[i]
            guard let brandID = (dic_car_brand["brand_id"] as? String) else{
                return
            }
            if arrcheckedM[i]{
                arr_selected_brand.append(brandID)
            }
            
        }
        
//        let stringRepresentation = array.joinWithSeparator("-") // "1-2-3"

        let str_brand = arr_selected_brand.joined(separator: ",")
        
        
        //------------ Create Car type array --------------
        var arr_selected_type : [String] = []
        for i in 0...self.arrCarType.count - 1{
            
            let dic_car_brand : [String : Any] = self.arr_car_types[i]
            guard let brandID = (dic_car_brand["car_id"] as? String) else{
                return
            }
            if arrCarType[i]{
                arr_selected_type.append(brandID)
            }
            
        }
        
        let str_type = arr_selected_type.joined(separator: ",")

        // * Chechk Whether Network is Unreachable *
        if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
            self.view.MyToast()
            
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        
        //    user_id:23
        //    transmission_type:both
        //    car_type:SVG
        //    price_range:1250
        //    brand_name:Tata
        //    device_unique_id:deviceuniqueid
        //    firebase_token:firebasetoken
        //    auth_key:camsapitaxi
        
        let parameters = ["user_id":"77",
                          "transmission_type":transmissionType,
                          "device_unique_id":get_deviceToken(),
                          "firebase_token":get_deviceToken(),
                          "device_type":"ios",
                          "auth_key":AUTH_KEY,
                          "car_type":str_type,
                          "brand_name":str_brand,
                          "price_range":price_range!] as [String : Any]
        print("body",parameters)
        let strUrl = kBaseURL + "driver_comfort" as String
        
        AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                progressHUD.hide(animated: true)
                print("response",response)

                if let value = response.value as? [String:Any] {
                    progressHUD.hide(animated: true)
                    print("response",value)
                    guard let status = value["status"] as? String else{
                        return
                    }
                    if status == "success"{
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login_VC") as! Login_VC
                        vc.modalPresentationStyle = .fullScreen
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    else{
                        guard let message = value["message"] as? String else{
                            return
                        }
                        //                            self.showAlert_with_OK(title: "Error", message:message )
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async() {
                    print(error.errorDescription?.description as Any)
                    progressHUD.hide(animated: true)
                    
                }
            }
        }
        
    }
    
    
}


extension YourComfort_VC : UICollectionViewDelegate,UICollectionViewDataSource{
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.arr_car_brands.count > 0{
            return arr_car_brands.count
        }
        else{
            return 0
        }
    }
    
    
    
    // make a cell for each cell index path
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Car_Brands_Cell", for: indexPath as IndexPath) as! Car_Brands_Cell
        
        cell.delegate = self
        let dic_car_brand : [String : Any] = self.arr_car_brands[indexPath.item]
        if let brandName = (dic_car_brand["brand_name"] as? String){
            cell.lbl_brand_value.text = brandName
        }
        
        if arrcheckedM.count > 0{
            
            if self.arrcheckedM[indexPath.item]{
                print("true",arrcheckedM[indexPath.item])
                let image: UIImage = UIImage(named: "tick_pink")!
                cell.view_btn_bg.backgroundColor = .white
                cell.btn_checked.layer.borderColor = UIColor.clear.cgColor
                cell.btn_checked .setImage(image, for: UIControl.State.normal)
            }
            else{
                print("false",arrcheckedM[indexPath.item])
                let image: UIImage = UIImage(named: "tick_circle")!
                cell.view_btn_bg.backgroundColor = .lightGray
                cell.btn_checked.layer.borderColor = UIColor.lightGray.cgColor
                cell.btn_checked.layer.borderWidth = 2
                cell.btn_checked .setImage(image, for: UIControl.State.normal)
                
            }
        }
        
        return cell
    }
    
    
//    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: (collectionView.frame.width / 2) - 10, height: 50) //add your height here
//    }
    
    
    func numberOfItemsInSection(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}


extension YourComfort_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.colView_brands.frame.width/2) - 10, height: 54)
    }
}


extension YourComfort_VC: CheckedCellDelegate {
    
    func checked(cell: Car_Brands_Cell) {
        let indexPath = self.colView_brands.indexPath(for: cell)
        print("Index",indexPath?.item)
        let cell = colView_brands!.cellForItem(at: indexPath!) as! Car_Brands_Cell
        let isChecked = arrcheckedM[indexPath!.item]
        
        if isChecked{
            let image: UIImage = UIImage(named: "tick_circle")!
            cell.view_btn_bg.backgroundColor = .lightGray
            cell.btn_checked.layer.borderColor = UIColor.lightGray.cgColor
            cell.btn_checked.layer.borderWidth = 2
            cell.btn_checked .setImage(image, for: UIControl.State.normal)
        }
        else{
            let image: UIImage = UIImage(named: "tick_pink")!
            cell.view_btn_bg.backgroundColor = .white
            cell.btn_checked.layer.borderColor = UIColor.clear.cgColor

            cell.btn_checked .setImage(image, for: UIControl.State.normal)
        }
        self.arrcheckedM[indexPath!.item] = !isChecked
        print(arrcheckedM)
    }
}
