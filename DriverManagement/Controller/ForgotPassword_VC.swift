//
//  ForgotPassword_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 15/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import MBProgressHUD
import Alamofire


class ForgotPassword_VC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_txtfeildBG: UIView!
    
    @IBOutlet weak var view_paging_unfocused: UIView!
    @IBOutlet weak var view_paging_focused: UIView!
    
    @IBOutlet weak var txtField_pwd: UITextField!
    @IBOutlet weak var txtFiled_otp: UITextField!
    @IBOutlet weak var txtField_confirmPwd: UITextField!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var view_confirmPassword: UIView!
    @IBOutlet weak var view_otp: UIView!
    @IBOutlet weak var view_bg: UIView!
    var user_id = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.setUIConfiguration()
    }
    
    
    
    func setUIConfiguration(){
        self.btn_submit.addSideShadow(color: .gray)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        view_otp.layer.cornerRadius = (self.view_otp.frame.height / 2)
        view_password.layer.cornerRadius = self.view_password.frame.height / 2
        view_confirmPassword.layer.cornerRadius = self.view_confirmPassword.frame.height / 2
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2

        
        view_otp.addShadowWithExtraWidth(color: .lightGray)
        view_confirmPassword.addShadowWithExtraWidth(color: .lightGray)
        view_password.addShadowWithExtraWidth(color: .lightGray)
        
        self.txtField_pwd.delegate = self
        self.txtFiled_otp.delegate = self
        self.txtField_confirmPwd.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_txtfeildBG
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_confirmPwd!.resignFirstResponder()
        txtFiled_otp!.resignFirstResponder()
        txtField_pwd!.resignFirstResponder()
    }
    
    @IBAction func submit_Action(_ sender: Any) {
        if txtFiled_otp.text == ""{
            showAlert_banner(msg: "OTP field cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else if txtField_pwd.text == "" || txtField_confirmPwd.text == ""{
            showAlert_banner(msg: "Password cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else if txtField_pwd.text != txtField_confirmPwd.text{
            showAlert_banner(msg: "Password should be same", bgColor: UIColor.red, type: "fail")
        }
        else{
            forgot_password_API()
        }
    }
    
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API
    //MARK:-
    func forgot_password_API()
    {
    
        let otp : String = self.txtFiled_otp.text!
        let password : String = self.txtField_confirmPwd.text!

        // * Chechk Whether Network is Unreachable *
        if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
            self.view.MyToast()
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        
        let parameters = ["code":otp,
                        "password":password,
                        "auth_key":AUTH_KEY,
                        "user_id":user_id] as [String : Any]
        
        let strUrl = kBaseURL + "forget_password" as String
    
        AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                progressHUD.hide(animated: true)
                if let value = response.value as? [String:Any] {
                    guard let status = value["status"] as? String else{
                        return
                    }
                    if status == "success"{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login_VC") as! Login_VC
                        vc.modalPresentationStyle = .fullScreen
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case .failure(let error):
                DispatchQueue.main.async() {
                    print(error.errorDescription?.description as Any)
                    progressHUD.hide(animated: true)
                    
                }
            }
        }

    }
    

        // -------- End Of API ---------
    
}
