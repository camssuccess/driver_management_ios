//
//  LocationTracking_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 19/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import Cosmos
import IHKeyboardAvoiding
import MapKit
import CoreLocation


class LocationTracking_VC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var view_profile_bottom: UIView!
    @IBOutlet weak var btn_profile_bottom: UIButton!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_review_txtBG: UIView!
    @IBOutlet weak var lbl_name_rating: UILabel!
    
    @IBOutlet weak var txtFeild_review: UITextField!
    @IBOutlet weak var view_give_rating: CosmosView!
    @IBOutlet weak var lbl_priceValue: UILabel!
    @IBOutlet weak var lbl_timeValue: UILabel!
    @IBOutlet weak var lbl_distanceValue: UILabel!
    @IBOutlet weak var btn_ptofile_rating: UIButton!
    @IBOutlet weak var view_rating_review: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    var currentLoc: CLLocation!
    let locationPicker = LocationPickerViewController()
    var pickUp_location_name = String()
    var pickUp_location = CLLocationCoordinate2D()
    
    var dropOff_location = CLLocationCoordinate2D()
    var dropOff_location_name = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        self.view_rating_review.isHidden = true
        // Do any additional setup after loading the view.
        
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }

        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true

        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        self.setUIConfiguration()
        
//        setCordination()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view
    }
    
    func setUIConfiguration(){
        btn_profile_bottom.contentMode = .scaleAspectFit
        btn_profile_bottom.layer.cornerRadius = self.btn_profile_bottom.frame.height / 2
        view_profile_bottom.layer.cornerRadius = self.view_profile_bottom.frame.height / 2
        
        //
        //---------------- Set Rating view -------------
        view_give_rating.rating = 2
        
        view_review_txtBG.addShadowWithExtraWidth(color: .lightGray)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        self.txtFeild_review.delegate = self
        
        btn_ptofile_rating.layer.cornerRadius = self.btn_ptofile_rating.frame.height / 2
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        
        view_review_txtBG.layer.cornerRadius = self.view_review_txtBG.frame.height / 2
        //        self.view_rating_review.roundCorners_AtTopRight(radius: 34)
        
        //----------------------------------------------
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)
        
        renderer.lineWidth = 5.0
        
        return renderer
    }
    
    
    func setCordination(){
        

        pickUp_location = CLLocationCoordinate2D(latitude: 26.846389, longitude:80.9616252)
        dropOff_location = CLLocationCoordinate2D(latitude: 24.832, longitude:78.9616252)

        // 1.
        mapView.delegate = self
        
        // 2.
        let sourceLocation = CLLocationCoordinate2D(latitude: pickUp_location.latitude, longitude: pickUp_location.longitude)
        let destinationLocation = CLLocationCoordinate2D(latitude: dropOff_location.latitude, longitude: dropOff_location.longitude)
        
        // 3.
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        // 5.
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = pickUp_location_name
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = dropOff_location_name
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        // 6.
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        //        mapView.icon = UIImage(named: "flag_icon")
        
        // 7.
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtFeild_review!.resignFirstResponder()
    }
    
    
    @IBAction func bottom_profile_Action(_ sender: Any) {
        self.view_rating_review.isHidden = false
    }
    
    
    @IBAction func submit_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home_VC") as! Home_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension LocationTracking_VC : MKMapViewDelegate,CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate

        mapView.mapType = MKMapType.standard

        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)//MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)

        let annotation = MKPointAnnotation()
        annotation.coordinate = locValue
        annotation.title = "Javed Multani"
        annotation.subtitle = "current location"
        mapView.addAnnotation(annotation)

        //centerMap(locValue)
    }
}
