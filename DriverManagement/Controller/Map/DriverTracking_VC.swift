//
//  DriverTracking_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 31/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import MapKit
import Cosmos
import IHKeyboardAvoiding
//import LocationPicker

struct Stadium {
    var name: String
    var lattitude: CLLocationDegrees
    var longtitude: CLLocationDegrees
}

class DriverTracking_VC: UIViewController,UITextFieldDelegate {
        
    @IBOutlet weak var view_details_bottom: UIView!
    @IBOutlet weak var btn_dropOff: UIButton!
    @IBOutlet weak var btn_pickUp: UIButton!
    
    @IBOutlet weak var view_dropOff: UIView!
    @IBOutlet weak var view_pickup: UIView!
    @IBOutlet weak var view_alert_top: UIView!
    var isCurrent_location : Bool = true
    var isPickUp_location : Bool = false
    var isDropOFF_location : Bool = false
    @IBOutlet weak var cons_alert_detail: NSLayoutConstraint!

    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_accept: UIButton!
    @IBOutlet weak var view_profile_details: UIView!
    //--------------------------------------------
    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet weak var lbl_estimated_time: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var view_rating: CosmosView!
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()

    var currentLoc: CLLocation!
    let locationPicker = LocationPickerViewController()
    var pickUp_location_name = String()
    var pickUp_location = CLLocationCoordinate2D()

    var dropOff_location = CLLocationCoordinate2D()
    var dropOff_location_name = String()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    if #available(iOS 13.0, *) {
        if traitCollection.userInterfaceStyle == .dark{
            overrideUserInterfaceStyle = .light
        }
    } else {
        // Fallback on earlier versions
    }
        
    self.isHidden_ViewDetails_bottom(val: false)
    }
    
    func isHidden_ViewDetails_bottom(val : Bool){
        view_details_bottom.isHidden = val
        view_profile_details.isHidden = val
    }
    
    func location_updated(){
        if !isDropOFF_location{
            dropOff_location_name = "No location selected"
        }
        
        
        btn_pickUp.setTitle(pickUp_location_name , for: .normal)
        btn_dropOff.setTitle(dropOff_location_name , for: .normal)
        
        if (isPickUp_location || isCurrent_location) && isDropOFF_location{
            view_dropOff.backgroundColor = .systemGreen
            cons_alert_detail.constant = (226/735) * screenHeight
            self.isHidden_ViewDetails_bottom(val: false)
        }
        else{
            view_dropOff.backgroundColor = .lightGray
            cons_alert_detail.constant = 0
            self.isHidden_ViewDetails_bottom(val: true)
        }
        
    }
    
    func selectLocation(location: CLLocation) {
        // add point annotation to map
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        mapView.addAnnotation(annotation)

        geocoder.cancelGeocode()
        geocoder.reverseGeocodeLocation(location) { response, error in
            if let error = error as NSError?, error.code != 10 { // ignore cancelGeocode errors
                // show error and remove annotation
                let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in }))
                self.present(alert, animated: true) {
                    self.mapView.removeAnnotation(annotation)
                }
            } else if let placemark = response?.first {
                // get POI name from placemark if any
                let name = placemark.areasOfInterest?.first

                // pass user selected location too
                self.location_pickUp = Location(name: name, location: location, placemark: placemark)
            }
        }
    }
    
    
    var location_pickUp: Location? {
        didSet {
            pickUp_location_name = location_pickUp.flatMap({ $0.title }) ?? "No location selected"
            print("pickUp_location_name",pickUp_location_name)
            btn_pickUp.setTitle(pickUp_location_name , for: .normal)
        }
    }
    
    

    func gotoLocationPickerController(){
        // you can optionally set initial location
//        var placemark = CLPlacemark()
//        let location = CLLocation(latitude: 0, longitude: 0)
//        let initialLocation = Location(name: "", location: location, placemark:placemark)
//        locationPicker.location = initialLocation
//
//        // button placed on right bottom corner
//        locationPicker.showCurrentLocationButton = true // default: true
//
//        // default: navigation bar's `barTintColor` or `UIColor.white`
//        locationPicker.currentLocationButtonBackground = .blue
//
//        // ignored if initial location is given, shows that location instead
//        locationPicker.showCurrentLocationInitially = true // default: true
//
//        locationPicker.mapType = .standard// default: .Hybrid
//
//        // for searching, see `MKLocalSearchRequest`'s `region` property
//        locationPicker.useCurrentLocationAsHint = true // default: false
//
//        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
//
//        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
//
//        // optional region distance to be used for creation region when user selects place from search results
//        locationPicker.resultRegionDistance = 500 // default: 600
//
//        locationPicker.completion = { location in
//
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        nc.addObserver(self, selector: #selector(userLoggedIn), name: Notification.Name("UserLoggedIn"), object: nil)
        
    }
    

    
    @objc func userLoggedIn(_ notification:Notification){
        // Do something now
         if let data = notification.userInfo as? [String: Any]
         {
            print("dropOff_location_name",data["dropOff_location_name"] as Any)
            
            if let pickUp_location_name = data["pickUp_location_name"]{
                self.pickUp_location_name = pickUp_location_name as! String
            }
            if let pickUp_location = data["pickUp_location"] as? CLLocationCoordinate2D{
                self.pickUp_location = pickUp_location
            }
            if let dropOff_location_name = data["dropOff_location_name"]{
                self.dropOff_location_name = dropOff_location_name as! String
            }
            if let dropOff_location = data["dropOff_location"]  as? CLLocationCoordinate2D{
                self.dropOff_location = dropOff_location
            }
            if let isPickUp_location = data["isPickUp_location"] as? Bool{
                self.isPickUp_location = isPickUp_location
            }
            if let isDropOFF_location = data["isDropOFF_location"] as? Bool{
                self.isDropOFF_location = isDropOFF_location
            }
            if let isCurrent_location = data["isCurrent_location"] as? Bool{
                self.isCurrent_location = isCurrent_location
            }
            self.location_updated()

         }
    }

    
    override func viewWillLayoutSubviews() {
//        self.isHidden_ViewDetails_bottom(val: true)
        self.setUIConfiguration()
        setCordination()
        setui_Alert_Location()
//        self.view_alertBG.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view
        gotoLocationPickerController()
        
   
    }
    
    func addBottomSheetView() {
        // 1- Init bottomSheetVC
        let bottomSheetVC = BottomSheetViewController()

//        // 2- Add bottomSheetVC as a child view
//        self.addChildViewController(bottomSheetVC)
//        self.view.addSubview(bottomSheetVC.view)
//        bottomSheetVC.didMoveToParentViewController(self)
//
//        // 3- Adjust bottomSheet frame and initial position.
//        let height = view.frame.height
//        let width  = view.frame.width
//        bottomSheetVC.view.frame = CGRectMake(0, self.view.frame.maxY, width, height)
    }
    

    
    
    func setui_Alert_Location(){
        btn_accept.layer.cornerRadius = self.btn_accept.frame.height / 2
        btn_cancel.layer.cornerRadius = self.btn_cancel.frame.height / 2
        view_profile_details.layer.cornerRadius = self.view_profile_details.frame.height / 2
        btn_accept.addSideShadow(color: .gray)
        btn_cancel.addSideShadow(color: .gray)
    }
    
    
    func setUIConfiguration(){
        self.btn_profile.contentMode = .scaleAspectFit
        //        self.btn_profile.addShadow(color: .gray)
        btn_profile.layer.cornerRadius = self.btn_profile.frame.height / 2
        btn_profile.layer.borderColor = UIColor.white.cgColor
        btn_profile.layer.borderWidth = 5
        
        view_rating.rating = 2
        
        //---------------- Set Rating view -------------
        view_pickup.layer.cornerRadius = self.view_pickup.frame.height / 2
        view_dropOff.layer.cornerRadius = self.view_dropOff.frame.height / 2
        view_alert_top.addSideShadow(color: .gray)
  
        //----------------------------------------------
    }
    
    func setCordination(){
 
        // 1.
        mapView.delegate = self
        
        // 2.
        let sourceLocation = CLLocationCoordinate2D(latitude: pickUp_location.latitude, longitude: pickUp_location.longitude)
        let destinationLocation = CLLocationCoordinate2D(latitude: dropOff_location.latitude, longitude: dropOff_location.longitude)
        
        // 3.
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        // 5.
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = pickUp_location_name
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = dropOff_location_name
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        // 6.
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
//        mapView.icon = UIImage(named: "flag_icon")

        // 7.
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
        
    }
    
    
    @IBAction func pickUp_clicked(_ sender: Any) {
        let locationPicker = self.storyboard?.instantiateViewController(withIdentifier: "LocationPickerViewController") as! LocationPickerViewController
        locationPicker.location = location_pickUp
        locationPicker.showCurrentLocationButton = true
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.selectCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.completion = { self.location_pickUp = $0 }
        locationPicker.isPickUp_location = true
        locationPicker.isDropOFF_location = false
        locationPicker.isCurrent_location = false
        locationPicker.pickUp_location_name = self.pickUp_location_name
        locationPicker.pickUp_location = self.pickUp_location
        locationPicker.dropOff_location_name = self.dropOff_location_name
        locationPicker.dropOff_location = self.dropOff_location
        // do some awesome stuff with location
        
        navigationController?.pushViewController(locationPicker, animated: true)
        
    }
    
    @IBAction func dropoff_clicked(_ sender: Any) {
        
        let locationPicker = self.storyboard?.instantiateViewController(withIdentifier: "LocationPickerViewController") as! LocationPickerViewController
        locationPicker.location = location_pickUp
        locationPicker.showCurrentLocationButton = true
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.selectCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.completion = { self.location_pickUp = $0 }
        
        locationPicker.pickUp_location_name = self.pickUp_location_name
        locationPicker.pickUp_location = self.pickUp_location
        locationPicker.dropOff_location_name = self.dropOff_location_name
        locationPicker.dropOff_location = self.dropOff_location
        locationPicker.isPickUp_location = false
        locationPicker.isDropOFF_location = true
        locationPicker.isCurrent_location = self.isCurrent_location
        // do some awesome stuff with location
        navigationController?.pushViewController(locationPicker, animated: true)
        
    }
    
    
    func getDirections(loc1: CLLocationCoordinate2D, loc2: CLLocationCoordinate2D) {
        let source = MKMapItem(placemark: MKPlacemark(coordinate: loc1))
        source.name = "Your Location"
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: loc2))
        destination.name = "Destination"
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)
        
        renderer.lineWidth = 5.0
        
        return renderer
    }
    
    
    //    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
    //
    //        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
    //        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
    //
    //        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
    //        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
    //
    //        let sourceAnnotation = MKPointAnnotation()
    //
    //        if let location = sourcePlacemark.location {
    //            sourceAnnotation.coordinate = location.coordinate
    //        }
    //
    //        let destinationAnnotation = MKPointAnnotation()
    //
    //        if let location = destinationPlacemark.location {
    //            destinationAnnotation.coordinate = location.coordinate
    //        }
    //
    //        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
    //
    //        let directionRequest = MKDirections.Request()
    //        directionRequest.source = sourceMapItem
    //        directionRequest.destination = destinationMapItem
    //        directionRequest.transportType = .automobile
    //
    //        // Calculate the direction
    //        let directions = MKDirections(request: directionRequest)
    //
    //        directions.calculate {
    //            (response, error) -> Void in
    //
    //            guard let response = response else {
    //                if let error = error {
    //                    print("Error: \(error)")
    //                }
    //
    //                return
    //            }
    //
    //            let route = response.routes[0]
    //
    //            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
    //
    //            let rect = route.polyline.boundingMapRect
    //            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
    //        }
    //    }
    
    @IBAction func submit_Action(_ sender: Any) {
//        self.view_rating_review.isHidden = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingConfirmed_VC")
        vc?.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    
    @IBAction func profile_clicked(_ sender: Any) {
        
//        self.view_rating_review.isHidden = false
    }
    
    @IBAction func submit_location(_ sender: Any) {
        
    }
    
    @IBAction func current_location(_ sender: Any) {
        
    }
    
    //-----------------   Action For Accept and Cancel ------------------
    
    @IBAction func acceept_Action(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingConfirmed_VC")
        vc?.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc!, animated: true)

        
//        cons_alert_detail.constant = 0
//        isHidden_ViewDetails_bottom(val: true)
//        view_rating_review.isHidden = false
//        view_alert_top.isHidden = true
        
    }
    
    @IBAction func cancel_Action(_ sender: Any) {
    }
    
    
    @IBAction func back_Action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension DriverTracking_VC : MKMapViewDelegate{
    
}

