//
//  LocationDemo.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 23/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

    import UIKit
    import Cosmos
    import IHKeyboardAvoiding
    import MapKit
    import CoreLocation


class LocationDemo: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var myMapView: MKMapView!
    var locationManager:CLLocationManager!
    let sourceLocation = CLLocationCoordinate2D(latitude: 40.759011, longitude: -73.984472)
    let destinationLocation = CLLocationCoordinate2D(latitude: 40.748441, longitude: -73.985564)

    
       
    @IBOutlet weak var lbl_distance: UILabel!
    override func viewDidLoad() {
           super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
           // Do any additional setup after loading the view, typically from a nib.
        self.setMapView()
       }
       
    

    func setMapView(){
        // 1.
        myMapView.delegate = self
        //For Location 1
        let location1 = CLLocationCoordinate2D(
            latitude: 51.481188400000010000,
            longitude: -0.190209099999947280
        )
        
        let annotation1 = MKPointAnnotation()
        annotation1.coordinate = location1;
        annotation1.title = "Chelsea"
        annotation1.subtitle = "Chelsea"
        
        let span = MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.15)//(0.15, 0.15)
        
        let region1 = MKCoordinateRegion(center: location1, span: span)
        myMapView.setRegion(region1, animated: true)
        myMapView.addAnnotation(annotation1)
        
        //For Location 2
        let location2 = CLLocationCoordinate2D(
            latitude: 51.554947700000010000,
            longitude: -0.108558899999934510
        )
        
        let annotation2 = MKPointAnnotation()
        annotation2.coordinate = location2;
        annotation2.title = "Arsenal"
        annotation2.subtitle = "Arsenal"
        
        let region2 = MKCoordinateRegion(center: location1, span: span)
        myMapView.setRegion(region2, animated: true)
        myMapView.addAnnotation(annotation2)
        
        let locations = [CLLocation(latitude: 51.481188400000010000, longitude: -0.190209099999947280), CLLocation(latitude: 51.554947700000010000,longitude:  -0.108558899999934510)]
        
        //This line shows error
        //             var coordinates = locations.map({(location: CLLocation) -> CLLocationCoordinate2D in return location.coordinate})
        var coordinates = locations.map {
            location in
            return location.coordinate
        }
        
        let polyline = MKPolyline(coordinates: &coordinates, count: locations.count)
        
        myMapView.addOverlay(polyline)
        
    }
  

    
       
       func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
       {
           print("Error \(error)")
       }
  
    
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }

        return nil
    }

    
    

}


extension LocationDemo : MKMapViewDelegate,CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let current_coordinates:CLLocationCoordinate2D = manager.location!.coordinate
        
        myMapView.mapType = MKMapType.standard

        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)//MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: current_coordinates, span: span)
        myMapView.setRegion(region, animated: true)

        let annotation = MKPointAnnotation()
        annotation.coordinate = current_coordinates
        annotation.title = "Me"
        annotation.subtitle = "current location"

        myMapView.addAnnotation(annotation)
        print("Location",current_coordinates)
        //centerMap(locValue)
    }
}
