//
//  About_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//
import IHKeyboardAvoiding
import UIKit
import Alamofire
import MBProgressHUD

class About_VC: UIViewController {

    @IBOutlet weak var btn_submit: UIButton!

    @IBOutlet weak var txtField_name: UITextField!
    @IBOutlet weak var view_paging_unfocused: UIView!
    @IBOutlet weak var view_paging_focused: UIView!

    @IBOutlet weak var txtField_PinCode: UITextField!
    @IBOutlet weak var txtFiled_mobile: UITextField!
    @IBOutlet weak var btn_dob: UIButton!
    @IBOutlet weak var view_code: UIView!
    @IBOutlet weak var view_mobile: UIView!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var view_username: UIView!
    @IBOutlet weak var view_bg: UIView!
    var user_id = String()
    let dob_placeholder = "Enter Date of birth"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        print("At About screen user id :",user_id)
        self.setUIConfiguration()
    }
    
    override func viewWillLayoutSubviews() {
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "CaviarDreams", size: 20)!]

    }
    

    func setUIConfiguration(){
        self.btn_submit.addSideShadow(color: .gray)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        view_mobile.layer.cornerRadius = (self.view_mobile.frame.height / 2) - 5
        view_username.layer.cornerRadius = self.view_username.frame.height / 2
        view_code.layer.cornerRadius = 10

        view_password.layer.cornerRadius = self.view_password.frame.height / 2
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2
        view_username.addShadowWithExtraWidth(color: .lightGray)
        view_password.addShadowWithExtraWidth(color: .lightGray)
        
        view_mobile.addShadowWithExtraWidth(color: .lightGray)
        view_code.addShadowWithExtraWidth(color: .lightGray)
//        self.txtField_name.delegate = self
        self.txtFiled_mobile.delegate = self
//        self.txtField_PinCode.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        self.btn_dob.setTitleColor(.lightGray, for: .normal)
        self.btn_dob.setTitle(dob_placeholder, for: .normal)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_name!.resignFirstResponder()
//        txtField_dob!.resignFirstResponder()
        txtFiled_mobile!.resignFirstResponder()
        txtField_PinCode!.resignFirstResponder()
    }
    
    @IBAction func submit_Action(_ sender: Any) {
        let userName : String = txtField_name.text!
        let dob : String = (self.btn_dob.titleLabel?.text)!
        let mobile : String = txtFiled_mobile.text!

        
        if userName == ""{
            showAlert_banner(msg: "Username cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        if userName.count > 8{
            showAlert_banner(msg: "Username must be less than or equal to 10 characters", bgColor: UIColor.red, type: "fail")
        }

        else if dob == dob_placeholder{
            showAlert_banner(msg: "Date of birth cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else if mobile == ""{
            showAlert_banner(msg: "Mobile number cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else if mobile.count > 10{
            showAlert_banner(msg: "Mobile must be 10 digit", bgColor: UIColor.red, type: "fail")
        }
        else{
            self.signUp_API_2()
            
        }
    }
    

      @IBAction func back_Action(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }
 
        

    
    @IBAction func dob_clicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Date", message: "Select date", preferredStyle: alertStyle)
        //        let alert = UIAlertController(style: self.alertStyle, title: "Date Picker", message: "Select Date")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: Date()) { date in
            print(date)
            //---------------string date from date --------------
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "dd-MM-yyyy"
            
            let strdate = dateFormatter.string(from: date)
            self.btn_dob.setTitle(strdate, for: .normal)
            self.btn_dob.setTitleColor(.black, for: .normal)
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
        
    }
    
        //MARK:- API
        //MARK:-
        
        func signUp_API_2()
        {
            
            let username : String = self.txtField_name.text!
            let mobile : String = self.txtFiled_mobile.text!
            let dob : String = (self.btn_dob.titleLabel?.text)!

            // * Chechk Whether Network is Unreachable *
            if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
                self.view.MyToast()
                return
            }
            
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.label.text = "Loading..."
            

            let parameters = ["user_id":user_id,
                            "name":username,
                            "dob":dob,
                            "mobile":mobile,
                            "auth_key":AUTH_KEY,
                            "device_type":"ios",
                            "device_unique_id":get_deviceToken(),
                            "firebase_token":get_deviceToken(),
                            "profile_pic":""] as [String : Any]
            let strUrl = kBaseURL + "sign_up_step2" as String
        
            AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    progressHUD.hide(animated: true)
                    print("response",response.value)

                    if let value = response.value as? [String:Any] {
                        let status = value["status"] as! String
                        if status == "success"{
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarificationCode_VC") as! VarificationCode_VC
                            vc.modalPresentationStyle  = .fullScreen
                            vc.user_id = self.user_id
                            vc.mobile_number = self.txtFiled_mobile.text!
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }

                case .failure(let error):
                    DispatchQueue.main.async() {
                        print(error.errorDescription?.description as Any)
                        progressHUD.hide(animated: true)
                        
                    }
                }
            }

        }
        

            // -------- End Of API ---------


}


extension About_VC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = txtFiled_mobile.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
