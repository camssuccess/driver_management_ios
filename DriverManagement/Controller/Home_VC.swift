//
//  Home_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 31/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class Home_VC: UIViewController {

    @IBOutlet weak var btn_appointment: UIButton!
    @IBOutlet weak var btn_wallets: UIButton!
    @IBOutlet weak var badge_rideLater: UIButton!
    @IBOutlet weak var badge_rideNow: UIButton!
    @IBOutlet weak var badge_towingCar: UIButton!

    @IBOutlet weak var view_rideLater: UIView!
    @IBOutlet weak var view_ride_now: UIView!
    @IBOutlet weak var view_towing_car: UIView!

    @IBOutlet weak var lbl_ride_notification_info: UILabel!
    
    @IBOutlet weak var lbl_driver_name: UILabel!
    @IBOutlet weak var lbl_towTruck: UILabel!{
        didSet{
        }
    }
    @IBOutlet weak var lbl_rideNow: UILabel!{
        didSet{
        }
    }
    @IBOutlet weak var lbl_rideLater: UILabel!{
        didSet{
        }
    }
    @IBOutlet weak var view_bg: UIView!
    var ride_now_noti : Int = 0
    var ride_later_noti : Int = 0
    var tow_car_noti : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        getHomeDetails_API()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getDriverInfo(){
        lbl_driver_name.text = getUserName()
        print("userName",getUserName())
        if self.ride_now_noti == 0{
            self.badge_rideNow.isHidden = true
        }
        if self.ride_later_noti == 0{
            self.badge_rideLater.isHidden = true
        }
        if self.tow_car_noti == 0{
            self.badge_towingCar.isHidden = true
        }

    }
    
    func setFont(){
        let font = UIFont(name: "Helvetica_Bold", size: 14)

        if isiPhone_SE{
            print("iphone SE")
            lbl_rideNow.font = font
            lbl_towTruck.font = font
            lbl_rideLater.font = font
        }
        else{
            print("Other iphone ")
        }
    }

    override func viewWillLayoutSubviews() {
        setUIConfiguration()
        setFont()
        
    }
    
    func setUIConfiguration(){
        self.view_bg.roundCorners_AtTopRight(radius: 34)
        self.view_ride_now.layer.cornerRadius = 10
        self.view_rideLater.layer.cornerRadius = 10
        self.view_towing_car.layer.cornerRadius = 10

        self.btn_wallets.layer.cornerRadius = 10
        self.btn_appointment.layer.cornerRadius = 10
        self.badge_rideNow.layer.cornerRadius = self.badge_rideNow.frame.height / 2
        self.badge_rideLater.layer.cornerRadius = self.badge_rideLater.frame.height / 2
        self.badge_towingCar.layer.cornerRadius = self.badge_towingCar.frame.height / 2
    }

    @IBAction func appointment_Action(_ sender: Any) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverTracking_VC") as! DriverTracking_VC
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RL_History_VC") as! RL_History_VC

        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func wallet_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Pandume_history_VC") as! Pandume_history_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ride_now_Action(_ sender: Any) {
        print("ride_now_Action")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverTracking_VC") as! DriverTracking_VC

        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ride_later_Action(_ sender: Any) {
        print("ride_later_Action")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RL_CarSelection_VC") as! RL_CarSelection_VC

        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func tow_car_Action(_ sender: Any) {
        print("tow_car_Action")

    }
    
        
        //MARK:- API
        //MARK:-
        func getHomeDetails_API()
        {
            // * Chechk Whether Network is Unreachable *
            if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
                self.view.MyToast()
                return
            }

            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.label.text = "Loading..."
            
            let parameters = ["auth_key":AUTH_KEY] as [String : Any]
            print("body",parameters)
            let strUrl = kBaseURL + "dashboard" as String
            
            AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    progressHUD.hide(animated: true)
                    if let value = response.value as? [String:Any] {
                        guard let status = value["status"] as? String else{
                            return
                        }
                        if status == "success"{
    //                        defaults.set(String(txtField_username.text!), forKey: USER_NAME)
                            if let dic_home = value["ride"] as? [String:Any] {
                                if let ride_now = dic_home["ride_now"] as? Int{
                                    self.ride_now_noti = ride_now
                                }
                                if let ride_later = dic_home["ride_later"] as? Int{
                                    self.ride_later_noti = ride_later
                                }
                                
                                if let tow_truck = dic_home["tow_truck"] as? Int{
                                    self.tow_car_noti = tow_truck
                                }
                                
                                if self.ride_now_noti > 0{
                                    self.badge_rideNow.isHidden = false
                                    self.badge_rideNow.setTitle(String(self.ride_now_noti), for: .normal)
                                }
                                else{
                                    self.badge_rideNow.isHidden = true

                                }
                                if self.ride_later_noti > 0{
                                    self.badge_rideLater.isHidden = false
                                    self.badge_rideLater.setTitle(String(self.ride_later_noti), for: .normal)

                                }
                                 else{
                                    self.badge_rideLater.isHidden = true
                                }
                                if self.tow_car_noti > 0{
                                    self.badge_towingCar.isHidden = false
                                    self.badge_towingCar.setTitle(String(self.tow_car_noti), for: .normal)
                                }
                                 else{
                                    self.badge_towingCar.isHidden = true

                                }

                            }
                        }
                        else{
                            showAlert_banner(msg: "Password didn't match", bgColor: UIColor.red, type: "fail")
                        }
                        
                    }
                    
                case .failure(let error):
                    DispatchQueue.main.async() {
                        print(error.errorDescription?.description as Any)
                        progressHUD.hide(animated: true)
                        
                    }
                }
            }
            
        }
}
