//
//  RideNowList_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 29/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class RideNowList_VC: UIViewController {

    @IBOutlet weak var tblView_rideNow_list: UITableView!
    var arr_selections_list : [[String:Any]] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        arr_selections_list = [
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Sachin","car_type":"Honda City","rating_count":4],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Rahul","car_type":"Beats","rating_count":3],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Gaurav","car_type":"Audi 5","rating_count":1],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Sachin","car_type":"Honda City","rating_count":4],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Rahul","car_type":"Beats","rating_count":3],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Gaurav","car_type":"Audi 5","rating_count":1],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Sachin","car_type":"Honda City","rating_count":4],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Rahul","car_type":"Beats","rating_count":3],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Gaurav","car_type":"Audi 5","rating_count":1]
        ]
        
        self.registerAllCell()

    }
    
    func registerAllCell(){

        let bundle = Bundle(for: type(of: self))
        let cellNib = UINib(nibName: "RideNow_Cell", bundle: bundle)
        tblView_rideNow_list.register(cellNib, forCellReuseIdentifier: "RideNow_Cell")
        
    }


}


extension RideNowList_VC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_selections_list.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideNow_Cell") as! RideNow_Cell
        let newDic = arr_selections_list[indexPath.item]
        
//        cell.lbl_name.text = (newDic["name"] as! String)
//        cell.lbl_car_type.text = (newDic["car_type"] as! String)
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 220
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Car_Confirmation_VC") as! Car_Confirmation_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
