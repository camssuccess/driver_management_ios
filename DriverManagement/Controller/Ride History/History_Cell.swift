//
//  History_Cell.swift
//  CustomerManagement
//
//  Created by KUMAR GAURAV on 08/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class History_Cell: UITableViewCell {
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_wheel_price: UILabel!
    @IBOutlet weak var lbl_payment_status: UILabel!
    @IBOutlet weak var view_bg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
