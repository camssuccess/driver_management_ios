//
//  RD_Confirmation.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 03/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class RL_Confirmation: UIViewController {

    
    
    @IBOutlet weak var view_txtFeild_dropOFF: UIView!

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var view_txtFeild_pickUP: UIView!
    @IBOutlet weak var view_txtFeild_starting: UIView!
    @IBOutlet weak var view_txtFeild_ending: UIView!
    @IBOutlet weak var view_time: UIView!
    @IBOutlet weak var view_selangor: UIView!
    @IBOutlet weak var view_slider: UIView!

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_car: UIImageView!
    @IBOutlet weak var lbl_car_name: UILabel!
    
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_confirm: UIButton!
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var View_upper_bg: UIView!
    @IBOutlet weak var lbl_date: UILabel!
    
    @IBOutlet weak var lbl_time: UILabel!
    
    @IBOutlet weak var lbl_description: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        self.setUIConfiguration()
    }
    
    func setUIConfiguration(){
        //          self.view_Alert_brands.isHidden = true
        
        self.btn_cancel.layer.cornerRadius = self.btn_cancel.frame.height / 2
        self.btn_confirm.layer.cornerRadius = self.btn_confirm.frame.height / 2
        self.view_bottom.layer.cornerRadius = 12
        self.View_upper_bg.layer.cornerRadius = 12

        self.view_time.layer.cornerRadius = self.view_time.frame.height / 2
        self.view_selangor.layer.cornerRadius = self.view_selangor.frame.height / 2
        self.view_slider.layer.cornerRadius = self.view_slider.frame.height / 2

        self.view_txtFeild_ending.layer.cornerRadius = self.view_txtFeild_ending.frame.height / 2
        self.view_txtFeild_pickUP.layer.cornerRadius = self.view_txtFeild_pickUP.frame.height / 2
        self.view_txtFeild_dropOFF.layer.cornerRadius = self.view_txtFeild_dropOFF.frame.height / 2
        self.view_txtFeild_starting.layer.cornerRadius = self.view_txtFeild_starting.frame.height / 2

//        self.view_bg.roundCorners_AtTopRight(radius: 34)
        

        slider.setThumbImage(image_dot_pink, for: .normal)
        slider.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
    }
    

    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func call_Action(_ sender: Any) {
    }
    
    @IBAction func message_Action(_ sender: Any) {
    }
    
}
