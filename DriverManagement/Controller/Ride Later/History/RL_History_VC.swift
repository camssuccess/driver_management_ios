//
//  RL_History_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 03/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import Kingfisher

class RL_History_VC: UIViewController {

    @IBOutlet weak var tblView_AppointmentList: UITableView!
    @IBOutlet weak var btn_SelectedDate: UIButton!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var btn_seeCars: UIButton!
    @IBOutlet weak var colView_Appointment: UICollectionView!
    var arr_appointments_list : [[String:Any]] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.setNavigationBarHidden(true, animated: animated)
     }
    
    override func viewWillLayoutSubviews() {
          setUIConfiguration()
      }
      
    func setUIConfiguration(){
        //          self.view_Alert_brands.isHidden = true
        
        btn_seeCars.layer.cornerRadius = self.btn_seeCars.frame.height / 2
        self.view_bg.roundCorners_AtTopRight(radius: 34)
        arr_appointments_list = [
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Sachin","car_type":"Honda City","rating_count":4],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Rahul","car_type":"Beats","rating_count":3],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Gaurav","car_type":"Audi 5","rating_count":1],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Sachin","car_type":"Honda City","rating_count":4],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Rahul","car_type":"Beats","rating_count":3],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Gaurav","car_type":"Audi 5","rating_count":1],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Sachin","car_type":"Honda City","rating_count":4],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Rahul","car_type":"Beats","rating_count":3],
            ["img":"https://res.cloudinary.com/rozgaarindia/image/upload/v1566366732/categorysample/d3nsikwrlbbxwkjpmz2r.jpg","name":"Gaurav","car_type":"Audi 5","rating_count":1]


        ]
        
    }

    @IBAction func clicked_seeCars(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Car_Confirmation_VC") as! Car_Confirmation_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func selectDate(_ sender: Any) {
        let alert = UIAlertController(title: "Date", message: "Select date", preferredStyle: alertStyle)
//        let alert = UIAlertController(style: self.alertStyle, title: "Date Picker", message: "Select Date")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: nil) { date in
            print(date)
            //---------------string date from date --------------

            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "dd-MM-yyyy"
            
            let strdate = dateFormatter.string(from: date)
            self.btn_SelectedDate.setTitle(strdate, for: .normal)
        }
        alert.addAction(title: "Done", style: .cancel)
        alert.show()
        
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension RL_History_VC : UICollectionViewDelegate,UICollectionViewDataSource{
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       return arr_appointments_list.count
    }
    
    
    
    // make a cell for each cell index path
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Appointment_Cell", for: indexPath as IndexPath) as! Appointment_Cell
    
        let newDic = arr_appointments_list[indexPath.item] 
        
        cell.lbl_name.text = (newDic["name"] as! String)
        cell.lbl_carType.text = (newDic["car_type"] as! String)
        
        cell.view_rating.rating = Double(newDic["rating_count"]as! Int)

        let strimg = newDic["img"] as! String
        cell.img_profile.kf.setImage(
            with: URL(string: strimg ),
            placeholder: UIImage(named: "no_image"))

        
        return cell
    }
    
    
//    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: (collectionView.frame.width / 2) - 10, height: 50) //add your height here
//    }
    
    
    func numberOfItemsInSection(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}


extension RL_History_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.colView_Appointment.frame.width/3) - 5, height: 190)
    }
}


extension RL_History_VC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_appointments_list.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentList_Cell") as! AppointmentList_Cell
        let newDic = arr_appointments_list[indexPath.item] 
        
        cell.lbl_name.text = (newDic["name"] as! String)
        cell.lbl_car_type.text = (newDic["car_type"] as! String)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RL_Confirmation") as! RL_Confirmation
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension RL_History_VC: TwoDotActionDelegate{
    func clickedTwoDot(cell: AppointmentList_Cell) {
        let indexPath = self.tblView_AppointmentList.indexPath(for: cell)
        print("index",indexPath?.row)

    }
}
