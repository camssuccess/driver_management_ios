//
//  Appointment_Cell.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 03/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import Cosmos

class Appointment_Cell: UICollectionViewCell {
    
    @IBOutlet weak var view_bg: UIView!{
        didSet{
            view_bg.layer.cornerRadius = 8
        }
    }
    @IBOutlet weak var view_rating: CosmosView!
    @IBOutlet weak var lbl_carType: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_profile: UIImageView!{
        didSet{
            img_profile.layer.cornerRadius = img_profile.frame.height / 2
            img_profile.layer.borderColor = UIColor.white.cgColor
            img_profile.layer.borderWidth = 2
            img_profile.contentMode = .scaleAspectFit
        }
    }
}
