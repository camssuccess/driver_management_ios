//
//  AppointmentList_Cell.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 03/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

protocol TwoDotActionDelegate {
    func clickedTwoDot(cell : AppointmentList_Cell)
}

class AppointmentList_Cell: UITableViewCell {

    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_completed: UILabel!{
        didSet{
            lbl_completed.layer.cornerRadius = lbl_completed.frame.height / 2
        }
    }
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_car_type: UILabel!

    @IBOutlet weak var view_bg: UIView!{
        didSet{
            view_bg.layer.cornerRadius = 10
        }
    }
    var delegate : TwoDotActionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func twoDotAction(_ sender: Any) {
        delegate?.clickedTwoDot(cell: self)
    }
}
