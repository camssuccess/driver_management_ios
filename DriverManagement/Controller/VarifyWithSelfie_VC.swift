//
//  VarifyWithSelfie_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MBProgressHUD
import Alamofire

class VarifyWithSelfie_VC: UIViewController,AVCaptureMetadataOutputObjectsDelegate,AVCapturePhotoCaptureDelegate {
    
    @IBOutlet weak var img_captured: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var camera_view: UIView!
    @IBOutlet weak var btn_veryfy: UIButton!
    var isMalasian : Bool = false
    var camera_screen = String()

    @IBOutlet weak var btn_ok_alert: UIButton!
    @IBOutlet weak var view_alert_bg: UIView!
    
    @IBOutlet weak var view_alert_main: UIView!
    //    var wrapper = OpenCVWrapper()
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var flipCamera : Bool = false
    var user_id = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        setUIConfiguration()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.view_alert_bg.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLayoutSubviews() {
        self.btn_veryfy.setTitle(CAPTUTRE_PHOTO, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        checkPermissions()
        // Setup your camera here...
        print("SCreenn",camera_screen)
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
//        let camera = getDevice(position: .front)
        setDevice(position: .front)
        
        if user_id == ""{
            user_id = "20"//get_user_id()
        }

    }
    
    //Get the device (Front or Back)
    func setDevice(position: AVCaptureDevice.Position){
        do {
            let camera = getDevice(position: position)

            if camera != nil{
                let input = try AVCaptureDeviceInput(device: camera!)
                //Step 9
                stillImageOutput = AVCapturePhotoOutput()
                
                
                if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                    captureSession.addInput(input)
                    captureSession.addOutput(stillImageOutput)
                    setupLivePreview()
                }
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    
    
    //Get the device (Front or Back)
    func getDevice(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devices() as NSArray;
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            if(deviceConverted.position == position){
               return deviceConverted
            }
        }
       return nil
    }

    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        camera_view.layer.addSublayer(videoPreviewLayer)
        
        
        let metadataOutput = AVCaptureMetadataOutput()
        if self.captureSession.canAddOutput(metadataOutput) {
            self.captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr, AVMetadataObject.ObjectType.ean13]
        } else {
            print("Could not add metadata output")
        }
        
        //Step12
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
             self.captureSession.startRunning()
             //Step 13
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.camera_view.bounds
            }
         }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)
        self.img_captured.image = image
        
    }



    override func viewWillLayoutSubviews() {
        
//        view_alert_bg.isHidden = true
        
        if camera_screen == IS_CAMERA_SELFIE_IC{
            lbl_title.text = "TAKE A SELFIE WITH YOUR IC"
        }
        else if camera_screen == IS_CAMERA_SELFIE_MY_KAD{
            lbl_title.text = "MALASIAN IC (MY KAD)"
        }
        else{
            lbl_title.text = "MALASIAN DRIVERS LICENSE"
        }
        
        setUIConfiguration()
        setNavigationBarTransparent()
    }
    
    
    func setNavigationBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        
    }

    func setUIConfiguration(){
        self.btn_veryfy.addSideShadow(color: .gray)
        btn_veryfy.layer.cornerRadius = self.btn_veryfy.frame.height / 2
        self.btn_ok_alert.addSideShadow(color: .gray)
        btn_ok_alert.layer.cornerRadius = self.btn_ok_alert.frame.height / 2
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ok_Alert_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectCar_VC") as! SelectCar_VC
        vc.modalPresentationStyle  = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func click_photo(_ sender: Any) {
        
        if self.btn_veryfy.titleLabel?.text == CAPTUTRE_PHOTO{
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
            stillImageOutput.capturePhoto(with: settings, delegate: self)
            self.btn_veryfy.setTitle(VERIFY, for: .normal)
        }
        else{
            self.upload_selfie_API()
        }
    }
    
    
    func showAlert_with_OK(title:String,message:String)  {
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
              
              let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarifyIdenty_VC") as! VarifyIdenty_VC
              vc.modalPresentationStyle  = .fullScreen
              self.navigationController?.pushViewController(vc, animated: true)
              
          }))

          self.present(alert, animated: true, completion: nil)
      }
    
    @IBAction func rotate_camera_Action(_ sender: Any) {
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        flipCamera = !flipCamera
        if flipCamera{
            setDevice(position: .back)
        }
        else{
            setDevice(position: .front)
        }
        
    }
    
}




extension VarifyWithSelfie_VC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage else { return }
        //        let scannerViewController = ImageScannerController(image: image, delegate: self)
        //        present(scannerViewController, animated: true)
    }
    
    
    //MARK:- API
    //MARK:-
    
    func upload_selfie_API()
    {
        
        var locality_type = String()
        if isMalasian{
            locality_type = "0"
        }
        else{
            locality_type = "1"
        }
        
        var verify_with = String()
        if camera_screen == IS_CAMERA_SELFIE_IC{
            verify_with = IS_CAMERA_SELFIE_IC
        }
        else if camera_screen == IS_CAMERA_SELFIE_MY_KAD{
            verify_with = IS_CAMERA_SELFIE_MY_KAD
            
        }
        else if camera_screen == IS_CAMERA_SELFIE_DRIVER_LICENSE{
            verify_with = IS_CAMERA_SELFIE_DRIVER_LICENSE
        }
        //        let user_type = String(0)

        let strImg = img_captured.image?.convertImageToBase64String()
        
        let parameters = ["user_id":user_id,
                          "user_type":"0",
                          "locality_type":locality_type,
                          "verification_with":verify_with,
                          "device_type":"ios",
                          "device_unique_id":get_deviceToken(),
                          "firebase_token":get_deviceToken(),
                          "selfie_image":strImg,
                          "auth_key":AUTH_KEY] as [String : Any]
        print(parameters)
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        
        let strUrl = kBaseURL + "selfie_with_ic_verification" as String
        AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                progressHUD.hide(animated: true)
                if let value = response.value as? [String:Any] {
                    let status = value["status"] as! String
                    if status == "success"{
                    
                        if self.camera_screen == IS_CAMERA_SELFIE_DRIVER_LICENSE{
                            self.view_alert_bg.isHidden = false
                        }
                            
                        else{
                            var verify_with = String()
                            if self.camera_screen == IS_CAMERA_SELFIE_IC{
                                verify_with = IS_CAMERA_SELFIE_MY_KAD
                            }
                            else if self.camera_screen == IS_CAMERA_SELFIE_MY_KAD{
                                verify_with = IS_CAMERA_SELFIE_DRIVER_LICENSE
                            }

                            let alert = UIAlertController(title: "Sucess", message: "Your Document has been submit successfuly", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarifyWithSelfie_VC") as! VarifyWithSelfie_VC
                                vc.isMalasian = false
                                print("")
                                vc.camera_screen = verify_with
                                vc.modalPresentationStyle  = .fullScreen
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async() {
                    print(error.errorDescription?.description as Any)
                    progressHUD.hide(animated: true)
                }
            }
        }
        
    }
    
    
    // -------- End Of API ---------
    
    
    
}
    
