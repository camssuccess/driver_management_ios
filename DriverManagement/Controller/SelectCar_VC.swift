//
//  SelectCar_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 28/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class SelectCar_VC: UIViewController {

    @IBOutlet weak var btn_selectNow: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        setUIConfiguration()
        setNavigationBarTransparent()
    }
    
    func setNavigationBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func setUIConfiguration(){
        self.btn_selectNow.addSideShadow(color: .gray)
        btn_selectNow.layer.cornerRadius = self.btn_selectNow.frame.height / 2
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
