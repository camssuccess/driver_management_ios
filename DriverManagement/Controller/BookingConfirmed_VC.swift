//
//  BookingConfirmed_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 02/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import Cosmos

class BookingConfirmed_VC: UIViewController {
    @IBOutlet weak var view_card: UIView!
    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet weak var lbl_id: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_type: UILabel!
    @IBOutlet weak var view_rating: CosmosView!
    @IBOutlet weak var slider_helpful: UISlider!
    @IBOutlet weak var lbl_review_count: UILabel!
    @IBOutlet weak var slider_Comfort: UISlider!
    @IBOutlet weak var slider_Friendly: UISlider!
    @IBOutlet weak var btn_msg: UIButton!
    @IBOutlet weak var btn_call: UIButton!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_bottom: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    override func viewWillLayoutSubviews() {

        setUIConfiguration()
    }
    

    func setUIConfiguration(){
        self.btn_submit.addSideShadow(color: .gray)
        self.view_card.addSideShadow(color: .gray)

        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        view_card.layer.cornerRadius = 20
        btn_profile.layer.cornerRadius = self.btn_profile.frame.height / 2
        self.view_bottom.roundCorners_AtTopRight(radius: 34)
        
        slider_Comfort.setThumbImage(image_dot_pink, for: .normal)
        slider_Comfort.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when
        
        slider_helpful.setThumbImage(image_dot_pink, for: .normal)
        slider_helpful.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when
        slider_Friendly.setThumbImage(image_dot_pink, for: .normal)
        slider_Friendly.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when
    }

    
    @IBAction func submit_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationTracking_VC") as! LocationTracking_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func msg_clicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MessageBoard") as! MessageBoard
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func call_clicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Calling_VC") as! Calling_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
