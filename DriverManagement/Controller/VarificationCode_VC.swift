//
//  VarificationCode_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import MBProgressHUD
import Alamofire

class VarificationCode_VC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var view_otp: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_paging_focused: UIView!
    @IBOutlet weak var view_paging_unfocused: UIView!
    
    @IBOutlet weak var btn_resned: UIButton!
    @IBOutlet weak var txtField_otp: UITextField!
    @IBOutlet weak var lbl_timer: UILabel!
    
    @IBOutlet weak var view_bg: UIView!
    var OTPTimer: Timer?
    var count = 120
    var mobile_number = String()
    var user_id = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        self.setUIConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
          navigationController?.setNavigationBarHidden(true, animated: animated)
      }
    
    
    
    override func viewWillLayoutSubviews() {
        
        OTPTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        self.btn_resned.isUserInteractionEnabled = false
        //        self.btn_resned.titleColor(.red for: .normal)
        //        self.btn_resned.isSelected = false
        
    }
    
    
    func setUIConfiguration(){
        self.btn_submit.addSideShadow(color: .gray)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        view_otp.layer.cornerRadius = (self.view_otp.frame.height / 2)
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2
        view_otp.addShadowWithExtraWidth(color: .gray)
        self.txtField_otp.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        
    }
    
    
    @objc func runTimedCode(){
        if(count > -1){
            let minutes = String(count / 60)
            var seconds = String(count % 60)
            if seconds == "0"{
                seconds = "00"
            }
            self.lbl_timer.text = "0" + minutes + ":" + seconds
            count = count - 1
        }
        else{
            self.btn_resned.isUserInteractionEnabled = true
//            self.btn_resned.isSelected = true
            
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_otp!.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg
    }
    
    
    @IBAction func resend_Action(_ sender: Any) {
        print("resend clicked")
    }
    
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submit_Action(_ sender: Any) {
        if txtField_otp.text == ""{
            showAlert_banner(msg: "OTP Field cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else{
//            self.otp_verification_API()
            showAlert_with_OK(title: "Success", message: "Mobile verify successfully")

        }
    }
    
    
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func showAlert_with_OK(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarifyIdenty_VC") as! VarifyIdenty_VC
            vc.user_id = self.user_id
            vc.modalPresentationStyle  = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
            
        }))

        self.present(alert, animated: true, completion: nil)
    }

    
    //MARK:- API
    //MARK:-
    
    func otp_verification_API()
    {
        
        let otp : String = self.txtField_otp.text!
        let device_token = defaults.value(forKey: DEVICE_TOKEN)
        // * Chechk Whether Network is Unreachable *
        if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
            self.view.MyToast()
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
//        user_id:23
//        code:1234
//        mobile:8596744152
//        device_type:android
//        device_unique_id:deviceuniqueid
//        firebase_token:firebasetoken
//        auth_key:camsapitaxi
        
        let parameters = ["user_id":user_id,
                          "code":otp,
                          "mobile":mobile_number,
                          "device_type":"ios",
                          "device_unique_id":get_deviceToken(),
                          "firebase_token":get_deviceToken(),
                          "auth_key":AUTH_KEY] as [String : Any]
        let strUrl = kBaseURL + "otp_verification" as String
        
        AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                progressHUD.hide(animated: true)
                if let value = response.value as? [String:Any] {
                    let status = value["status"] as! String
                    if status == "success"{
                        guard let user_status = value["user_status"] as? Int else{
                            return
                        }
                        defaults.set(user_status, forKey: SIGNUP_STATUS)
                        defaults.synchronize()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarifyIdenty_VC") as! VarifyIdenty_VC
                        vc.modalPresentationStyle  = .fullScreen
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async() {
                    print(error.errorDescription?.description as Any)
                    progressHUD.hide(animated: true)
                }
            }
        }
        
    }
    
    
    // -------- End Of API ---------
}
