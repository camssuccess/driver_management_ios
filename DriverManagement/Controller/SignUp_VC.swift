//
//  SignUp_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 25/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import Alamofire
import MBProgressHUD


class SignUp_VC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtField_password: UITextField!
    @IBOutlet weak var txtField_username: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var btn_google: UIButton!
    @IBOutlet weak var btn_twitter: UIButton!
    @IBOutlet weak var btn_facebook: UIButton!
    @IBOutlet weak var view_bg: UIView!
    var isFirstTime : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        self.setUIConfiguration()
    }
    

    func setUIConfiguration(){
        self.btn_google.contentMode = .scaleAspectFit
        self.btn_submit.addShadow(color: .gray)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        btn_google.layer.cornerRadius = self.btn_google.frame.height / 2
        btn_twitter.layer.cornerRadius = self.btn_twitter.frame.height / 2
        btn_facebook.layer.cornerRadius = self.btn_facebook.frame.height / 2
        self.txtField_password.delegate = self
        self.txtField_username.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        setNavigationBarTransparent()
        
        txtField_username.setIcon(UIImage(named: "user")!)
        txtField_password.setIcon(UIImage(named: "password")!)
    }
    
    func setNavigationBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg

        if get_user_id() != ""  && isFirstTime{
            
            print(get_user_id())
            isFirstTime = false
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "About_VC") as! About_VC
              vc.modalPresentationStyle = .fullScreen
            vc.user_id = get_user_id()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_username!.resignFirstResponder()
        txtField_password!.resignFirstResponder()
    }

    @IBAction func facebook_Action(_ sender: Any) {
    }
    
    @IBAction func google_Action(_ sender: Any) {
    }
    
    @IBAction func twitter_Action(_ sender: Any) {
    }
    

  

    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func submit_Action(_ sender: Any) {
        let userName : String = txtField_username.text!
        let password : String = txtField_password.text!
        print(password.isAlphanumeric)

        if userName == ""{
            showAlert_banner(msg: "Username cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else if userName.count > 8{
            showAlert_banner(msg: "Username must be 8 characters", bgColor: UIColor.red, type: "fail")
        }
        else if password == ""{
            showAlert_banner(msg: "Password cannot be empty", bgColor: UIColor.red, type: "fail")
        }
        else if (password.isAlphanumeric){
            showAlert_banner(msg: "Password must contain 8 charactera including upper and lower case letters and a number.", bgColor: UIColor.red, type: "fail")
        }
        else if (password.count > 10){
            showAlert_banner(msg: "Password must be Minimum 8 and Maximum 10 characters allow.", bgColor: UIColor.red, type: "fail")
        }
        else{
            signUp_API()
//            let username : String = self.txtField_username.text!
//            let password : String = self.txtField_password.text!
//            let strUrl = kBaseURL + "sign_up_step1" as String

//            AF.request("\(strUrl)&username=\(username)&password=\(password)&login_type=\(0)&user_type=\(0)").responseJSON {[weak self] response in
//                if let json = response.result as? [String: Any]{
//                    print(json)
//                    if (json["status"] as! NSString) == "success"{
//                        print("Success")
//
//                    }
//                    else{
//                        print("Failed")
//
//                    }
//                }
//            }//End
        }

        
    }
    
        //MARK:- API
        //MARK:-
    
    func signUp_API()
    {
        

        let username : String = self.txtField_username.text!
        let password : String = self.txtField_password.text!
        guard let device_token = defaults.value(forKey: DEVICE_TOKEN) as? String else {
            return
        }
        // * Chechk Whether Network is Unreachable *
        if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
            self.view.MyToast()
            
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
//        username:testnewuser
//        password:testnewpassword
//        login_type:1
//        user_type:0
//        device_type:android
//        device_unique_id:deviceuniqueidhere
//        firebase_token:firebasetokenhere
//        auth_key:camsapitaxi
        
        let parameters = ["username":username,
                        "password":password,
                        "device_unique_id":get_deviceToken(),
                        "firebase_token":get_deviceToken(),
                        "device_type":"ios",
                        "auth_key":AUTH_KEY,
                        "login_type":0,
                        "user_type":0] as [String : Any]
        print("body",parameters)
        let strUrl = kBaseURL + "sign_up_step1" as String
    
        AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                progressHUD.hide(animated: true)
                if let value = response.value as? [String:Any] {
                    progressHUD.hide(animated: true)
                    print("response",value)
                    guard let status = value["status"] as? String else{
                        return
                    }
                    if status == "success"{
                        guard let user_id = value["user_id"] as? Int else{
                            return
                        }
                        let strUSER_ID = String(user_id)
                        defaults.set(user_id, forKey: USER_ID)
                        defaults.synchronize()
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "About_VC") as! About_VC
                        vc.modalPresentationStyle = .fullScreen
                        vc.user_id = strUSER_ID
                        self.navigationController?.pushViewController(vc, animated: true)
                        print("got it")
                        
                    }
                    else{
                        guard let message = value["message"] as? String else{
                            return
                        }
                        self.showAlert_with_OK(title: "Error", message:message )
                    }
                    
                }

            case .failure(let error):
                DispatchQueue.main.async() {
                    print(error.errorDescription?.description as Any)
                    progressHUD.hide(animated: true)
                    
                }
            }
        }

    }
    
    func showAlert_with_OK(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))

        self.present(alert, animated: true, completion: nil)
    }

    
        // -------- End Of API ---------
    
}
