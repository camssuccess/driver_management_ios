//
//  Car_Brands_Cell.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 02/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

protocol CheckedCellDelegate : AnyObject {
    func checked(cell : Car_Brands_Cell)
}

class Car_Brands_Cell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_brand_value: UILabel!
    @IBOutlet weak var btn_checked: UIButton!{
        didSet{
            btn_checked.layer.borderWidth = 2
            btn_checked.layer.borderColor = UIColor.lightGray.cgColor
            btn_checked.layer.cornerRadius = btn_checked.frame.height / 2

        }
    }
    
    var delegate : CheckedCellDelegate?
    
    @IBOutlet weak var view_btn_bg: UIView!{
        didSet{
            view_btn_bg.layer.cornerRadius = view_btn_bg.frame.height / 2
        }
    }
    @IBAction func clicked_btn(_ sender: Any) {
        delegate?.checked(cell: self)
    }
}
