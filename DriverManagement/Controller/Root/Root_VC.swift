//
//  Root_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 27/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class Root_VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark{
                overrideUserInterfaceStyle = .light
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Do any additional setup after loading the view.
        if (defaults.value(forKey: IS_LOGGED_IN) != nil){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home_VC") as! Home_VC
            vc.modalPresentationStyle  = .fullScreen
            self.present(vc, animated: true, completion: nil)
//            self.navigationController?.pushViewController(vc, animated: true)

        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login_VC") as! Login_VC
            vc.modalPresentationStyle  = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
//            self.present(vc, animated: true, completion: nil)

        }

    }

    override func viewWillAppear(_ animated: Bool) {
          navigationController?.setNavigationBarHidden(true, animated: animated)

      }

}
