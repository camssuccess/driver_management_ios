//
//  UIImage.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 25/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import Foundation
import UIKit

extension UIImage{
    
    func convertImageToBase64String () -> String {
        return self.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }

}
