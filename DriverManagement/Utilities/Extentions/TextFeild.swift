//
//  TextFeild.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 24/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 0, y: 5, width: 15, height: 15))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 50, y: 0, width: 25, height: 25))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
}
