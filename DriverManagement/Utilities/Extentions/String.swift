//
//  String.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 24/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}"
            , options: .regularExpression) == nil
    }
    
    func convertBase64StringToImage () -> UIImage {
        let imageData = Data.init(base64Encoded: self, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }


}
